@if(Auth::guard('super_admin')->check())
<nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
      <div class="avatar"><img src="/Admin/freelancer/profile_pic/{{Session::get('AdminProfilePic')}}" alt="..." class="img-fluid rounded-circle"></div>
      <div class="title">
        <h1 class="h5">{{Session::get('Username')}}</h1>
        <p>Admin</p>
      </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
            <li><a href="/admin/dashboard"> <i class="icon-home"></i>Home </a></li>
<li class="active"><a href="/admin/freelancer_list"> <i class="icon-grid"></i>Freelancer</a></li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Lead tracker</a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="/admin/request_call_back_lists">Request A Callback</a></li>
                <li><a href="/admin/get_quote_lists">Get a Quote</a></li>
              </ul>
            </li>
            <li><a href="/admin/add_freelancer"> <i class="icon-user-1"></i>Add New Freelancer</a></li>
            <li><a href="/admin/change_password"> <i class="fa fa-key"></i>Change Password</a></li>

            <li><a href="/admin/admin_logout"> <i class="icon-logout"></i>Log out </a></li>
    </ul>

  </nav>
@elseif(Auth::guard('user')->check())

<nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
      <div class="avatar"><img src="/Admin/freelancer/profile_pic/{{Session::get('UserProfilePic')}}" alt="..." class="img-fluid rounded-circle"></div>
      <div class="title">
        <h1 class="h5">{{Session::get('Username')}}</h1>
        <p>Freelancer</p>
      </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
            <li class="active"><a href="/admin/freelancer/dashboard"> <i class="icon-home"></i>Home </a></li>
<!--<li><a href="works.html"> <i class="icon-grid"></i>Lead tracker</a></li>-->
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Lead tracker</a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="/admin/freelancer/request_call_back_lists">Request A Callback</a></li>
                <li><a href="/admin/freelancer/get_quote_lists">Get a Quote</a></li>
              </ul>
            </li>
            <li><a href="/admin/freelancer/edit_profile"> <i class="icon-user-1"></i>Edit User Profile</a></li>
            <li><a href="/admin/freelancer/change_password"> <i class="fa fa-key"></i>Change Password</a></li>

            <li><a href="/admin/freelancer/user_logout"> <i class="icon-logout"></i>Log out </a></li>
    </ul>

  </nav>
  @endif