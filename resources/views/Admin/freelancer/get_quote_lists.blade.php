@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Get a Quote List</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              
              <div class="table-responsive"> 
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Email Id</th>
                      <th>Contact No</th>
                      <th>City/State/Country</th>
                      <th>Service</th>
                      <th>For Whom</th>
                      <th>Decision Maker</th>
                      <th>Budget</th>
                      <th>Connect</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($GetQuote)
                    @foreach($GetQuote as $Quote)
                    <tr>
                      <th scope="row">1</th>
                        <td>{{$Quote->name}}</td>
                      <td>{{$Quote->email}}</td>
                      <td>{{$Quote->contact}}</td>
                      <td>{{$Quote->city}}/{{$Quote->state}}/{{$Quote->country}}</td>
                      <td>{{$Quote->service}}</td>
                      <td>{{$Quote->your_client}}</td>
                      <td>{{$Quote->requirement}}</td>
                      <td>{{$Quote->budget}}</td>
                      <td>{{$Quote->questions_requirement}}</td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
          
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection