@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Request a Callback List</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              
              <div class="table-responsive"> 
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Email Id</th>
                      <th>Contact Number</th>
                      <th>City/State/Country</th>
                      <th>Requirement</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(($RequestCallBack))
                    @foreach($RequestCallBack as $CallBack)
                    <tr>
                      <th scope="row">1</th>
                      <td>{{$CallBack->name}}</td>
                      <td>{{$CallBack->email}}</td>
                      <td>{{$CallBack->contact}}</td>
                      <td>{{$CallBack->city}}/{{$CallBack->state}}/{{$CallBack->country}}</td>
                      <td>{{$CallBack->requirements}}</td>
                      <!--<td>
                        <a href="#" class="btn button-sm red">Pending..</a>
                        <a href="edit_freelancer.html" class="btn button-sm blue">Edit</a>
                     </td>-->
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
          
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection