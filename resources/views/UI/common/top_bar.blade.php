<div id="top-bar" class="d-none d-md-block">

    <div class="container clearfix">

        <div class="col_half nobottommargin">

            <div id="logo">
                <a href="/" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="{{URL::asset('UI/images/logo.png')}}" alt="Canvas Logo"></a>
                <a href="/" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="{{URL::asset('UI/images/logo@2x.png')}}" alt="Canvas Logo"></a>
            </div>
            {{-- <p class="nobottommargin"><strong>Call:</strong> 86184 14801 | <strong>Email:</strong> info@accountswale.com</p> --}}

        </div>

        <div class="col_half col_last fright nobottommargin">

            <!-- Top Links
            ============================================= -->
            <div class="top-links">
                @if(Auth::guard('user')->check())
                    <ul class="not-dark align-self-end">
                        <li>Go to profile</li>
                        <li><a href="http://freelancer.taprecruiter.com/accountswale/register"><i class="icon-registered1"></i> {{Session::get('Username')}}</a></li>
                    </ul>
                @else
                    <ul class="not-dark align-self-end">
                        <li>For a freelancer </li>
                        <li><a href="http://freelancer.taprecruiter.com/accountswale/register"><i class="icon-registered1"></i> Register</a></li>
                        <li><a href="http://freelancer.taprecruiter.com/users/login" class="color"><div><i class="icon-line2-login"></i> Login</div></a></li>
                    </ul>

                @endif
                <!--<ul>

                    <li><a href="#">Register</a></li>
                    <li><a href="#">Sign In</a>
                        <div class="top-link-section">
                            <form id="top-login">
                                <div class="input-group" id="top-login-username">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="icon-user"></i></div>
                                    </div>
                                    <input type="email" class="form-control" placeholder="Email address" required="">
                                </div>
                                <div class="input-group" id="top-login-password">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="icon-key"></i></div>
                                    </div>
                                    <input type="password" class="form-control" placeholder="Password" required="">
                                </div>
                                <label class="checkbox">
                                  <input type="checkbox" value="remember-me"> Remember me
                                </label>
                                <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                            </form>
                        </div>
                    </li>-->
                </ul>
            </div><!-- .top-links end -->

        </div>

    </div>

</div>
