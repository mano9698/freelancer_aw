<header id="header">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            {{-- <div id="logo">
                <a href="/" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="{{URL::asset('UI/images/logo.png')}}" alt="Canvas Logo"></a>
                <a href="/" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="{{URL::asset('UI/images/logo@2x.png')}}" alt="Canvas Logo"></a>
            </div> --}}
            <!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">

                {{-- <ul>
                    <li><a href="#"><div class="services_menu">Services</div></a>
                        <ul>
                            <li><a href="#"><div>Company Registration </div></a></li>
                            <li><a href="#"><div>GST Registration </div></a></li>
                            <li><a href="#"><div>Income Tax Filing </div></a></li>
                            <li><a href="#"><div>Accounting </div></a></li>
                            <li><a href="#"><div>PF ESI Consulting </div></a></li>
                            <li><a href="#"><div>Private Equity </div></a></li>
                            <li><a href="#"><div>Business Advisor </div></a></li>
                        </ul>
                    </li>
                    <li><a href="#"><div>About Us</div></a></li>
                    <li><a href="#"><div>Jobs</div></a></li>
                </ul> --}}

            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header>
