<footer id="footer" class="dark">

    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        {{-- <div class="footer-widgets-wrap clearfix">

            <div class="col_two_third">

                <div class="col_one_third">

                    <div class="widget clearfix">
                        <h4>Follow Us</h4>
                            <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                        <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                        <a href="#" class="social-icon si-dark si-colored si-pinterest nobottommargin" style="margin-right: 10px;">
                                <i class="icon-pinterest"></i>
                                <i class="icon-pinterest"></i>
                            </a>
                        <a href="#" class="social-icon si-dark si-colored si-linkedin nobottommargin" style="margin-right: 10px;">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                    </div>

                </div>

                <div class="col_one_third col_last">
            <div class="widget widget_links clearfix">

                        <h4>Informations</h4>

                        <ul>
                            <li><a href="about.html">About Us</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="return.html">Return</a></li>
                            <li><a href="privacy.html">Privacy Policy</a></li>
                            <li><a href="terms.html">Terms Of Use</a></li>
                        </ul>

                    </div>
            </div>

            </div>

            <div class="col_one_third col_last">


<div class="widget clearfix">

<h4>Locations</h4>

                            <address>
                                3rd Floor, Kiran Tower , Service Road, <br>
                                Teacher's Colony, Sector 5, HSR Layout, Near Silk Board Junction,<br>
                                Bengaluru, Karnataka 560034.
                            </address>
                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 86184 14801<br>
                            <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:info@accountswale.in">info@accountswale.in</a>


                    </div>




            </div>

        </div> --}}

    </div>

    <!-- Copyrights
    ============================================= -->

    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half footer-font-copyright">
                © 2020 <a href="/">Accountswale</a>. All rights reserved.<br>

            </div>

            <div class="col_half col_last tright footer-font-copyright">

                <i class="icon-envelope2"></i> info@accountswale.in <span class="middot">·</span> <i class="icon-headphones"></i> +91-99166 69702
            </div>

        </div>

    </div>

    {{-- <div id="copyrights">

        <div class="container clearfix">


© 2020 <a href="index.html">Accountswale</a>. All rights reserved.


        </div>

    </div> --}}
    <!-- #copyrights end -->

</footer><!-- #footer end -->
