<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;
use App\Models\UI\RequestCallBack;
use App\Models\UI\GetQuote;
use Session;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function login(){
        $title ="Admin Login";

        return view('Admin.layouts.login', compact('title'));
    }

    public function dashboard(){
        $title ="Dashboard";

        return view('Admin.layouts.dashboard', compact('title'));
    }

    public function freelancer_list(){
        $title ="Request Call Back";
        $Users = Users::where('user_type', 2)->get();
        return view('Admin.layouts.freelancer_lists', compact('title', 'Users'));
    }

    public function add_freelancer(){
        $title ="Add Freelancer";
        // $Users = Users::get();
        return view('Admin.layouts.add_freelancer', compact('title'));
    }

    public function request_call_back_lists(){
        $title ="Request Call Back";
        // $UserId = Session::get('UserId');
        $RequestCallBack = RequestCallBack::select('request_call_back.*', 'users.first_name', 'users.last_name', 'users.email as UserEmail')
                            ->join('users', 'users.id', 'request_call_back.user_id')
                            ->get();
        return view('Admin.layouts.request_call_back_lists', compact('title', 'RequestCallBack'));
    }

    public function get_quote_lists(){
        $title ="Get Quote";
        // $UserId = Session::get('UserId');
        $GetQuote = GetQuote::select('get_quote.*', 'users.first_name', 'users.last_name', 'users.email as UserEmail')
        ->join('users', 'users.id', 'get_quote.user_id')
        ->get();
        return view('Admin.layouts.get_quote_lists', compact('title', 'GetQuote'));
    }

    public function edit_freelancer($id){
        $title ="Edit Freelancer";
        $Users = Users::where('id', $id)->first();
        // echo json_encode($Users);
        // exit;
        return view('Admin.layouts.edit_freelancer', compact('title', 'Users'));
    }

    public function store_freelancer(Request $request){

        // $UserId = Session::get('UserId');

        $Users = new Users;

        // echo $request->contact_no;
        // exit;

        $Users->first_name = $request->first_name;
        $Users->last_name = $request->last_name;
        $Users->email = $request->email;
        $Users->password = Hash::make($request->password);
        $Users->contact_no = $request->contact_no;
        $Users->gender = $request->gender;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;
        $Users->offers = $request->offers;
        $Users->area = $request->area;
        $Users->qualification = $request->qualification;
        $Users->experience_years = $request->experience_years;
        $Users->experience_months = $request->experience_months;
        $Users->skills = $request->skills;
        // $Users->skills = explode(",", $request->skills);
        $Users->status = 1;
        $Users->user_type = 2;

        if($request->hasfile('profile_pic')){
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $dir = 'Admin/freelancer/profile_pic';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('profile_pic')->move($dir, $filename);
    
            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Updated Successfully');

    }

    public function update_freelancer(Request $request){

        $UserId = $request->id;

        $Users = Users::where('id', $UserId)->first();

        // echo $request->contact_no;
        // exit;

        // $Users->first_name = $request->first_name;
        // $Users->last_name = $request->last_name;
        // $Users->email = $request->email;
        // $Users->password = Hash::make($request->password);
        $Users->contact_no = $request->contact_no;
        $Users->gender = $request->gender;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;
        $Users->offers = $request->offers;
        $Users->area = $request->area;
        $Users->qualification = $request->qualification;
        $Users->experience_years = $request->experience_years;
        $Users->experience_months = $request->experience_months;
        $Users->skills = $request->skills;
        // $Users->skills = explode(",", $request->skills);
        // print_r(explode(',',$request->skills,-1));
        // exit;

        if($request->hasfile('profile_pic')){
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $dir = 'Admin/freelancer/profile_pic';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('profile_pic')->move($dir, $filename);
    
            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Updated Successfully');

    }

    public function changeStatus(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->status = $request->status;
        $user->save();
  
        return response()->json(['success'=>'Status change successfully.']);
    }

    public function change_password(){
        $title ="Change Password";
        $UserId = Session::get('UserId');
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.layouts.change_password', compact('title'));
    }


    public function update_password(Request $request)
    {
 
        //  $this->validate($request, [
 
        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);
 
        $AdminId = Session::get('AdminId');
 
       $hashedPassword = Auth::guard('super_admin')->user()->password;
 
       if (\Hash::check($request->oldpassword , $hashedPassword )) {
 
         if (!\Hash::check($request->newpassword , $hashedPassword)) {
 
              $users =Users::find($AdminId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $AdminId)->update( array( 'password' =>  $users->password));
 
              session()->flash('message','password updated successfully');
              return redirect()->back();
            }
 
            else{
                  session()->flash('message','new password can not be the old password!');
                  return redirect()->back();
                }
 
           }
 
          else{
               session()->flash('message','old password doesnt matched ');
               return redirect()->back();
             }
 
       }

    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        return redirect('/admin/login');
    }
}
