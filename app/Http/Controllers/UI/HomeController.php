<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;

class HomeController extends Controller
{
    public function home(){
        $title ="::Welcome to Accountswale | Home::";

        $Users = Users::distinct()->get();

        return view('UI.layouts.home', compact('title', 'Users'));
    }
}
