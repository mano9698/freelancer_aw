<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\RequestCallBack;
use App\Models\UI\GetQuote;

use Session;

class FreelancerController extends Controller
{
    public function freelancer_lists(Request $request){
        $title ="::Welcome to Accountswale | Freelancer::";

        // $Country = $request->country;
        // $State = $request->state;
        // $City = $request->city;

        // echo $Country;
        // exit;

        // $Users = Users::where('country', 'LIKE', "%{$Country}%")
        //                 ->Where('state', 'LIKE', "%{$State}%")
        //                 ->Where('city', 'LIKE', "%{$City}%")
        //                 ->get();
        $GetUsers = json_decode(file_get_contents("http://freelancer.taprecruiter.com/api/freelancers/list/4"), true);

        $Users = $GetUsers['data'];
        // $Users = Users::where('user_type', 2)->where('status', 1)->get();
        
        // echo json_encode($Users);
        // exit;

        return view('UI.freelancer.search_freelancer', compact('title', 'Users'));
    }

    public function search_freelancer(Request $request){
        $title ="::Welcome to Accountswale | Freelancer::";

        $Country = $request->country;
        $State = $request->state;
        $City = $request->city;
        $Area = $request->area;
        $Gender = $request->gender;
        $UsertType = 3;

        $request->session()->put('Country', $request->country);
        $request->session()->put('State', $request->state);
        $request->session()->put('City', $request->city);
        $request->session()->put('Area', $request->area);
        $request->session()->put('Gender', $request->gender);
        
        // echo Session::get('Country');
        // exit;
        $GetUsers = json_decode(file_get_contents("http://freelancer.taprecruiter.com/api/freelancers/search?country=$Country&state=$State&city=$City&area=$Area&gender=$Gender&user_type=$UsertType"), true);

        // $GetUsers = json_decode(file_get_contents("http://freelancer.taprecruiter.com/api/freelancers/search?country=India&state&city=bangalore&area&gender&user_type=3"), true);

        $Users = $GetUsers['data'];
        // $Users = Users::where('country', 'LIKE', "%{$Country}%")
        //                 ->Where('state', 'LIKE', "%{$State}%")
        //                 ->Where('city', 'LIKE', "%{$City}%")
        //                 ->Where('area', 'LIKE', "%{$Area}%")
        //                 ->Where('gender', 'LIKE', "%{$Gender}%")
        //                 ->Where('user_type', 2)
        //                 ->where('status', 1)
        //                 ->get();


        return view('UI.freelancer.search_freelancer', compact('title', 'Users'));
    }


    public function request_call_back($id){
        $title ="Request Call Back";

        return view('UI.freelancer.request_call_back', compact('title'));
    }

    public function store_request_call_back(Request $request){
        

        $RequestCallBack = new RequestCallBack();

        $RequestCallBack->user_id = $request->user_id;
        $RequestCallBack->name = $request->name;
        $RequestCallBack->email = $request->email;
        $RequestCallBack->contact = $request->contact;
        $RequestCallBack->country = $request->country;
        $RequestCallBack->state = $request->state;
        $RequestCallBack->city = $request->city;
        $RequestCallBack->requirements = $request->requirements;

        $AddRequestCallBack = $RequestCallBack->save();

        return redirect()->back()->with('message','Thank you for considering my profile for your requirement  and submitting details to help me call you back . I shall revert to you as soon as possible');

    }

    public function get_quote($id){
        $title ="Get A Quote";

        return view('UI.freelancer.get_quote', compact('title'));
    }

    public function store_get_quote(Request $request){
        

        $GetQuote = new GetQuote();

        $GetQuote->user_id = $request->user_id;
        $GetQuote->name = $request->name;
        $GetQuote->email = $request->email;
        $GetQuote->contact = $request->contact;
        $GetQuote->country = $request->country;
        $GetQuote->state = $request->state;
        $GetQuote->city = $request->city;
        $GetQuote->service = $request->service;
        $GetQuote->your_client = $request->your_client;
        $GetQuote->requirement = $request->requirement;
        $GetQuote->budget = $request->budget;
        $GetQuote->questions_requirement = $request->questions_requirement;

        $AddGetQuote = $GetQuote->save();

        return redirect()->back()->with('message','Thank you for considering my profile for your requirement  and submitting details to help me send your quote . I shall revert to you as soon as possible');

    }
}
